import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Home from '../../pages/Home/Home';
import AppHeader from '../AppHeader/AppHeader';
import Film from '../../pages/Film/Film.js';
import About from '../../pages/About/About.js'

function App() {
  return (
    <Router>
      <div className="App">
      <AppHeader />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/film/:slug" exact component={Film} />
          <Route path="/about" exact component={About} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;

import React from 'react';

class SimilarFilm extends React.Component{
    render(){
        const { similarFilms } = this.props;
        console.log(similarFilms);
        if(similarFilms.length === 0){
            return(
                <p> Aucun film similaire trouvé... </p>
            );
        }

        return(
            <div>
                {similarFilms.map((movie, i) => {
                    return(
                        <div>
                            <p key={i}> {movie.title}  </p>
                            <p key={i}> {movie.releaseYear}  </p>    
                        </div>
                    );
                })}
            </div>
        );
    };
}

export default SimilarFilm;
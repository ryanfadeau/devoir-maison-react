import React from 'react';
import FilmPreview from '../../components/FilmPreview/FilmPreview.js';

class Feed extends React.Component{
    render(){
        const { films } = this.props;
        return(
            <section>
                {films.map((film, i) => {
                    return (
                        <section key={i}>
                            <FilmPreview
                                title = {film.title}
                                releaseYear = {film.releaseYear}
                                slug = {film.slug}
                            />
                        </section>
                    );
                })}
            </section>
        );
    }
}

export default Feed;
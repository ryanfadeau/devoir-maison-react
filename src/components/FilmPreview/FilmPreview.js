import React from 'react';
import { Link } from 'react-router-dom';

class FilmPreview extends React.Component{
    render(){
        const { title, releaseYear, slug } = this.props;
        return(
            <div>
                <Link to={'/film/' + slug}> 
                    <h1> {title} </h1>
                </Link>
                <p> {releaseYear} </p>
            </div>
        );
    }
}

export default FilmPreview;
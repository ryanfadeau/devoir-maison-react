import React from 'react';
import { getOneFilm } from '../../api.js';
import SimilarFilm from '../../components/SimilarFilm/SimilarFilm.js';

class Film extends React.Component{


    constructor(props){
        super(props);

        this.state = {
            description: '',
            releaseYear: '',
            similarFilms: [],
            slug: '',
            title: '', 
            isNotFound: false
        }
    }

    componentDidMount(){
        const { match } = this.props;
        const slug = match.params.slug;
        const film = getOneFilm(slug);
        
        if(film === null){
            this.setState({ isNotFound: true });
        } else {
            this.setState({
                description: film.description,
                releaseYear: film.releaseYear,
                similarFilms: film.similarFilms,
                slug: slug,
                title: film.title
            });
        }


        

    }

    render(){
        const { description, releaseYear, similarFilms, slug, title, isNotFound } = this.state;
        if(isNotFound === true){
            return(<h1>NOT FOUND....</h1>);
        }


        return(
            <section>
                <h1> { title } </h1>
                <p> { description } </p>
                <p> { releaseYear } </p>
                <h1> Films similaires </h1>
                <SimilarFilm similarFilms={similarFilms}/>
            </section>
        );
    }

}

export default Film;
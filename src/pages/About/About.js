import React from 'react';
import monstres_cie from '../../assets/monstres_cie.jpg';

class About extends React.Component{
    render(){
        return(
            <section>
                <h1> Monstres & company </h1>
                <p>
                    Monstropolis est une petite ville peuplée de monstres dont la principale source d'énergie provient des cris des enfants. 
                    Monstres & Cie est la plus grande usine de traitement de cris de la ville. 
                    Grâce au nombre impressionnant de portes de placards dont dispose l'usine, une équipe de monstres d'élite pénètre dans le monde des humains pour terrifier durant la nuit les enfants et récolter leurs hurlements.
                </p>
                <p> Oui, j'ai 20 ans. Je suis un gamin. </p>
                <p> <img src={monstres_cie}></img> </p>
            </section>
        );
    }
}

export default About;
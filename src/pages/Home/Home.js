import React from 'react';
import { getAllFilms } from '../../api.js';
import Feed from '../../components/Feed/Feed.js'

export default class Home extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      films: []
    }
  }

  componentDidMount(){

    const films = getAllFilms();
    this.setState({
      films: films
    });
    console.log(films);
    
  }

  render() {
    const { films } = this.state;
    return (
      <div>
        <h1>Tous nos films</h1>
        <Feed films = {films} />
      </div>
    );
  }
}
